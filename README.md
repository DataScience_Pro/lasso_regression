# README #

This is Lasso + XGB Regression Project.

### Lasso Regression Description

LASSO stands for Least Absolute Shrinkage and Selection Operator.
Lasso is a regression analysis method that performs both variable selection and regularization in order to enhance the prediction accuracy and interpretability of the statistical model it produces.
Two key point of Lasso is "Absolute" and "Selection".
The optimization objective for Lasso is:

    (1 / (2 * n_samples)) * ||y - Xw||^2_2 + alpha * ||w||_1

Technically the Lasso model is optimizing the same objective function as the Elastic Net with l1_ratio=1.0 (no L2 penalty).