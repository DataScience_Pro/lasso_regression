from sklearn.linear_model import Lasso
import numpy as np
import pandas as pd

train_data = pd.read_csv('E:/data1.csv', sep=',')
x_data = []
v = train_data.values
for i in range(0, len(v)):
    x_data.append(v[i][0:len(train_data.keys()) - 5])
X = np.array(x_data)
Y = np.array(train_data['Next High Price'])
lasso = Lasso(alpha=0.1)
model = lasso.fit(X, Y)
print(model.predict([v[0][0:len(train_data.keys()) - 5]]))
